package ejerciciojava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
	static Map<String, Integer> contactos = new TreeMap<>();

	public static void main(String[] args) throws IOException {
		
		BufferedReader linea = new BufferedReader(new InputStreamReader(System.in));
		
		do{
			System.out.print("> ");
		}while (ejecutarComando(linea.readLine()));

	}
	static boolean ejecutarComando(String cmd){
		Scanner in = new Scanner(cmd);
		in.useDelimiter(":");
		String s = in.next(); 
		if(s.equals("buscar")){
			String nombre = in.next();
			Integer telefono = contactos.get(nombre);
			if(telefono!=null){
				System.out.printf("Telefono: %d\n",telefono);
			}else{
				System.out.println("Contacto no encontrado");
			}
		}else if (s.equals("eliminar")){
			String nombre = in.next();
			Integer telefono = contactos.get(nombre);
			if(telefono==null){
				System.out.println("Contacto no encontrado");
			}
		}else if (s.equals("salir")){
			return false;
		}else{
			try{
				int telefono = in.nextInt();
				Integer anterior = contactos.put(s,telefono);
				if (anterior != null){
					System.out.printf("El telefono %d se ha cambiado por el telefono %d\n",anterior,telefono);
				}
			} catch(InputMismatchException e){
				System.out.println("No has introducido un telefono");
			}
			
		}
		in.close();
		return true;
	}

}
